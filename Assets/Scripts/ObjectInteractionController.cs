using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInteractionController : MonoBehaviour
{
    private Vector3 startPosition;
    private Quaternion startRotation;
    private Vector3 startScale;

    //the SerializeField tag makes these fields visable in the inspector
    [SerializeField] private GameObject meshObject = null;
    [SerializeField] private GameObject pointCloudObject = null;

    /// <summary>
    /// Gets called automatically at the start of the application
    /// 
    /// Stores the transform pos, rot, scale to their respective variables
    /// </summary>
    private void Start()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;
        startScale = transform.localScale;
    }

    /// <summary>
    /// Gets called by the reset button in the scene
    /// 
    /// It sets the position, rotation and scale to the values stored at the start
    /// </summary>
    public void ResetObject()
    {
        transform.SetPositionAndRotation(startPosition, startRotation);
        transform.localScale = startScale;
    }

    /// <summary>
    /// Gets called by the swap button in the scene
    /// 
    /// First checks if the mesh object is active
    /// then it disables the mesh object and enables the pointcloud object
    /// 
    /// if the mesh object is not active it does the opposite
    /// </summary>
    public void SwapMeshToPointCloud()
    {
        //Don't do anything if the objects aren't set
        if(meshObject == null || pointCloudObject == null)
        {
            return;
        }

        if(meshObject.activeSelf == true)
        {
            meshObject.SetActive(false);
            pointCloudObject.SetActive(true);
        }
        else
        {
            meshObject.SetActive(true);
            pointCloudObject.SetActive(false);
        }
    }
}
